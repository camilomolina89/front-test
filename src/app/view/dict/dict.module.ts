import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DictRoutingModule } from './dict-routing.module';
import { DictComponent } from './dict.component';


@NgModule({
  declarations: [DictComponent],
  imports: [
    CommonModule,
    DictRoutingModule
  ]
})
export class DictModule { }
