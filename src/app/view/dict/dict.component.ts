import { Component, OnInit } from '@angular/core';
import { ClientService } from 'src/app/client.service';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-dict',
  templateUrl: './dict.component.html',
  styleUrls: ['./dict.component.scss']
})
export class DictComponent implements OnInit {

  constructor( private client: ClientService) { }
  data: any[];
  extraCols = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ]
  loading: boolean;
  ngOnInit() {
  }
  descargar() {
    this.loading = true
    const request = this.client.get(environment.urlDict);
    request.pipe(
      map( data => JSON.parse(data)),
      map( data => data.map(item => item.paragraph)),
    )
    .subscribe( data => {
      this.data = this.parse(data);
      this.loading = false
    });
  }
  private parse( data ) {
    return data.map( item => {
      const response: any = {};
      const letters = [...item].map( element => element.toUpperCase())
      this.extraCols.forEach( letter => response[letter] = 0);
      response.sum = 0;
      Object.keys(response).forEach(key => {
        response[key] = letters.filter(element => element === key).length;
      });
      const numbers = item.match(/\d+/g)
      if (numbers) {
        response.sum = numbers
          .map( num => Number(num))
          .map( num => Math.abs(num))
          .reduce((accumulator, currVal) => accumulator + currVal, 0);
      }
      response.text = item;
      return response;
    });
  }

}
