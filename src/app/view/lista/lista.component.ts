import { Component, OnInit } from '@angular/core';
import { ClientService } from 'src/app/client.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {
  loading: boolean;
  data: Number[];
  constructor(private client: ClientService) { }

  ngOnInit() {
  }
  descargar() {
    const request = this.client.get(environment.urlLista);
    this.loading = true
    request
    .subscribe( data => {
      this.data = this.parse(data);
      this.loading = false;
    });
  }
  private parse( data ) {

    const list = data.filter( (item, index) => data.indexOf(item) === index);
    return list.map( item => {
      const response: any = {};
      response.number = item,
      response.quantity = data.filter( el => el === item).length;
      response.firstPosition = data.indexOf(item);
      response.lastPosition = data.lastIndexOf(item);
      return response;
    });
  }

}
