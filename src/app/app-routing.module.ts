import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'lista',
    loadChildren: () => import('src/app/view/lista/lista.module').then(module =>  module.ListaModule),
  },
  {
    path: 'dict',
    loadChildren: () => import('src/app/view/dict/dict.module').then(module =>  module.DictModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
