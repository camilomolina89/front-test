export interface ApiResponse {
    data: any,
    error: string | any,
    success: boolean,
}
