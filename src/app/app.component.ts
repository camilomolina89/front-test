import { Component } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Prueba técnica';
  currentUrl;
  constructor( private router: Router) {
    this.router.events
      .subscribe( ev => {
        if (ev instanceof NavigationStart) {
          console.log(ev);
          this.currentUrl = ev.url
        }
      });
  }
}
