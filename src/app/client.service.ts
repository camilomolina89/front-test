import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { catchError, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ApiResponse } from './interfaces/api-response';
@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) {

  }

  get( url ) {
    return this.http.get(url)
    .pipe(
      catchError(this.errorHandler),
      map((response: ApiResponse) => response.data),
    )
  }
  private errorHandler(error) {
    return Observable.throw(error);
  }
}
